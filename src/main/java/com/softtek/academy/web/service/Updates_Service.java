package com.softtek.academy.web.service;
import java.text.ParseException;
import java.io.IOException;

public interface Updates_Service {
	int update()throws ParseException, IOException;
}