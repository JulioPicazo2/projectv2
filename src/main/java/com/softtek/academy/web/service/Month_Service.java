package com.softtek.academy.web.service;

import com.softtek.academy.web.model.Months;

public interface Month_Service {
	//Para obtener las horas por mes y IS
	Months hoursBy_Is_n_Month(String _strIs, int _mes);
}
