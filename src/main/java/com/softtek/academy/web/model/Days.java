package com.softtek.academy.web.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
@Entity
public class Days {
	@Id
    @GeneratedValue
    private int _id;
    private String _strIs; 
    private String _strDate;
    private String _strCin;
    private String _strCout;
    private long _seconds;
    private int _minutes;
    
	public Days(int _id, String _strIs, String _strDate, String _strCin, String _strCout, long _seconds, int _minutes) {
		super();
		this._id = _id;
		this._strIs = _strIs;
		this._strDate = _strDate;
		this._strCin = _strCin;
		this._strCout = _strCout;
		this._seconds = _seconds;
		this._minutes = _minutes;
	}

	public Days() {
		super();
	}

	public int get_id() {
		return _id;
	}

	public void set_id(int _id) {
		this._id = _id;
	}

	public String get_strIs() {
		return _strIs;
	}

	public void set_strIs(String _strIs) {
		this._strIs = _strIs;
	}

	public String get_strDate() {
		return _strDate;
	}

	public void set_strDate(String _strDate) {
		this._strDate = _strDate;
	}

	public String get_strCin() {
		return _strCin;
	}

	public void set_strCin(String _strCin) {
		this._strCin = _strCin;
	}

	public String get_strCout() {
		return _strCout;
	}

	public void set_strCout(String _strCout) {
		this._strCout = _strCout;
	}

	public long get_seconds() {
		return _seconds;
	}

	public void set_seconds(long _seconds) {
		this._seconds = _seconds;
	}

	public int get_minutes() {
		return _minutes;
	}

	public void set_minutes(int _minutes) {
		this._minutes = _minutes;
	}

	@Override
	public String toString() {
		return "Days [_id=" + _id + ", _strIs=" + _strIs + ", _strDate=" + _strDate + ", _strCin=" + _strCin
				+ ", _strCout=" + _strCout + ", _seconds=" + _seconds + ", _minutes=" + _minutes + "]";
	} 
}
