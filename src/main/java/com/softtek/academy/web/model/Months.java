package com.softtek.academy.web.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
@Entity
public class Months {
	@Id
	@GeneratedValue
    private int _id;
    private String _strIs;
    private int _mes;
    private long _seconds;
    private int _minutes;
    
	public Months(int _id, String _strIs, int _mes, long _seconds, int _minutes) {
		super();
		this._id = _id;
		this._strIs = _strIs;
		this._mes = _mes;
		this._seconds = _seconds;
		this._minutes = _minutes;
	}

	public Months() {
		super();
	}

	public int get_id() {
		return _id;
	}

	public void set_id(int _id) {
		this._id = _id;
	}

	public String get_strIs() {
		return _strIs;
	}

	public void set_strIs(String _strIs) {
		this._strIs = _strIs;
	}

	public int get_mes() {
		return _mes;
	}

	public void set_mes(int _mes) {
		this._mes = _mes;
	}

	public long get_seconds() {
		return _seconds;
	}

	public void set_seconds(long _seconds) {
		this._seconds = _seconds;
	}

	public int get_minutes() {
		return _minutes;
	}

	public void set_minutes(int _minutes) {
		this._minutes = _minutes;
	}

	@Override
	public String toString() {
		return "Months [_id=" + _id + ", _strIs=" + _strIs + ", _mes=" + _mes + ", _seconds=" + _seconds + ", _minutes="
				+ _minutes + "]";
	}
    
}
