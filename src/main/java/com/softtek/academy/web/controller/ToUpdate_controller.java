package com.softtek.academy.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.io.IOException;
import java.text.ParseException;

import com.softtek.academy.web.service.Updates_Service;

@RestController
@RequestMapping("api/v1")
public class ToUpdate_controller {
	
	@Autowired
	private Updates_Service updateService;
	
    @RequestMapping("/load")
    public String toLoad() throws ParseException, IOException {
    	updateService.update();
        return "Already ->"; 
    }
}
