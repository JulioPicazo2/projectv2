package com.softtek.academy.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.softtek.academy.web.model.Days;
import com.softtek.academy.web.service.Days_Service;

@RestController
@RequestMapping("api/v1")
public class Day_controller {
	@Autowired
	Days_Service days_Service;

	@GetMapping(path =  "/days", produces = "application/json")
	public List<Days> dev(){
		return days_Service.getalll();
	}
	
	@RequestMapping("/")
	    public String index() {
	        return "win";
	        //System.out.println("");
	}
	
}
