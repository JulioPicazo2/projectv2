package com.softtek.academy.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.softtek.academy.web.model.Months;
import com.softtek.academy.web.service.Month_Service;

@RestController
@RequestMapping("api/v1")
public class Months_controller {
	
	@Autowired
	private Month_Service month_Service;

	@GetMapping(path =  "/users/{_strIs}/{_mes}", produces = "application/json")
	public Months IsAndMonth(@PathVariable String _strIs, @PathVariable int _mes) {
		System.out.println("win" + _strIs +  " " + _mes);
		System.out.println();
		return month_Service.hoursBy_Is_n_Month(_strIs, _mes);
	}
}
