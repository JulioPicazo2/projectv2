package com.softtek.academy.web.soap;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableTransactionManagement
@EnableJpaAuditing

@SpringBootApplication(scanBasePackages = "com.softtek.academy.web")

@EntityScan("com.softtek.academy.web.model")
@EnableJpaRepositories("com.softtek.academy.web.repository")
public class FinalApplication {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SpringApplication.run(FinalApplication.class, args);
	}

}
