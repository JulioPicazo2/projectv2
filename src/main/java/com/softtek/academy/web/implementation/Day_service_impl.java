package com.softtek.academy.web.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.softtek.academy.web.model.Days;
import com.softtek.academy.web.repository.Day_repository;
import com.softtek.academy.web.service.Days_Service;

@Service
public class Day_service_impl implements Days_Service{
	
	@Autowired
	private Day_repository dayRepository;

	@Override
	public List<Days> getalll() {
		// TODO Auto-generated method stub
		return dayRepository.findAll();
	}
}
