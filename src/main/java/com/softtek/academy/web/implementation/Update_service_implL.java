package com.softtek.academy.web.implementation;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.softtek.academy.web.model.Days;
import com.softtek.academy.web.model.Months;
import com.softtek.academy.web.repository.Day_repository;
import com.softtek.academy.web.repository.Month_repository;
import com.softtek.academy.web.service.Updates_Service;
import com.softtek.academy.web.soap.GetMonthInfo;

@Service
public class Update_service_implL implements Updates_Service{
	@Autowired
	private Day_repository dayRepository;
	
	@Autowired
	private Month_repository monthRepository;

	@Override
	public int update() throws ParseException, IOException {
		// TODO Auto-generated method stub
		GetMonthInfo info = new GetMonthInfo();
    	System.out.println("Crea objeto");
    	Object[] obj = info.setMonthInfo();
    	info.SetMonthInfoExcel();
    	List<Days> days = (List<Days>) obj[0];
    	
    	
    	List<Months> months = (List<Months>) obj[1];
    	dayRepository.deleteAll();
    	dayRepository.saveAll(days);
    	monthRepository.deleteAll();
    	
    	
    	monthRepository.saveAll(months);
		return 0;
	}

}
