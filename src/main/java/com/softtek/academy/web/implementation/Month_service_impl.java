package com.softtek.academy.web.implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.softtek.academy.web.model.Months;
import com.softtek.academy.web.repository.Month_repository;
import com.softtek.academy.web.service.Month_Service;

@Service
public class Month_service_impl implements Month_Service {
	@Autowired
	private Month_repository monthRepository;

	@Override
	public Months hoursBy_Is_n_Month(String _strIs, int _mes) {
		// TODO Auto-generated method stub
		return monthRepository.find_IsAndMonth(_strIs, _mes);
	}

}
