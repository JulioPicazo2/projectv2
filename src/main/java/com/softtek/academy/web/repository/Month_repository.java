package com.softtek.academy.web.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.softtek.academy.web.model.Months;

@Repository
public interface Month_repository extends JpaRepository<Months, Integer>{
	//
	Months findFirst_Isstr(String _strIs);
	Months find_IsAndMonth(String _strIs, int month);
}