package com.softtek.academy.web.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.softtek.academy.web.model.Days;

@Repository("daysRepository")
@Transactional
public interface Day_repository extends JpaRepository<Days, Integer>{
	Days findFirstByIsstr(String isstr);
}
